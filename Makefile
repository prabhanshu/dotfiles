DIR="~/dotfiles"

.PHONY: all

all: i3 zsh  emacs  bash slim config install

i3: $DIR/.i3
	ln --backup -s $DIR/.i3 ~/
	sudo ln -sf $DIR/.i3/i3status.conf /etc/i3status.conf

emacs:$DIR/.emacs.d
	ln --backup -s $DIR/.emacs.d ~/
	sudo ln --backup -sf $DIR/.emacs.d /root/
vim:
	ln --backup -sf $DIR/.vimrc ~/
	ln --backup -sf $DIR/.gvimrc ~/	
	sudo ln --backup -sf $DIR/.vimrc /root/
	sudo ln --backup -sf $DIR/.gvimrc /root/

thunderbird:$DIR/.thunderbird
	ln --backup -sf $DIR/.thunderbird ~/

bash:$DIR/.bashrc
	ln --backup -sf $DIR/.bashrc ~/
	ln --backup -sf $DIR/.bash_profile ~/
	sudo ln --backup -sf $DIR/.bashrc /root/
	sudo ln --backup -sf $DIR/.bash_profile /root/

slim:$DIR/slim
	ln --backup -s $DIR/slim/slim.conf /etc/slim.conf
	sudo ln --backup -sf $DIR/slim/theme /usr/share/slim/theme

#config:$DIR/.config
#	ln --backup -s $DIR/.config ~/.config

install:
	sudo pacman -S unzip openssh xclip git zathura zathura-pdf-poppler feh scrot wget

zsh:$DIR/.zshrc
	ln --backup -sf $DIR/.zshrc ~/
	sudo ln --backup -sf $DIR/.zshrc /root/
	sudo ln --backup -sf $DIR/.zsh_profile /root
	ln --backup -sf $DIR/.zsh_profile ~/

config:
	ln --backup -sf $DIR/.inputrc ~/
	ln --backup -sf $DIR/.xinitrc ~/
	ln --backup -sf $DIR/.Xresources ~/
	ln --backup -sf $DIR/.conkyrc ~/
	ln --backup -sf $DIR/Makefile ~/
	sudo ln --backup -sf $DIR/.inputrc /root/
	sudo ln --backup -sf $DIR/.xinitrc /root/
	sudo ln --backup -sf $DIR/.Xresources /root/






