#COMMANDS and NOTES
>commands and notes from various books sites or whichever i found useful for me.
Mainly about Bash,Zsh,wget,and other linux stuffs
-------
>Bash_commands : bash environment variables,options,files read during startup ,special Bash variables ,and file descriptors ,with trick for no output or error display in terminal.

>Bash_script : necessary stuffs for writing bash script.

>zsh_commands: trick for no output or error display in terminal,no globbing

>wget_commands: daily use wget commands

>keyboard_commands: some commands to set/reset your keyboard keys.

>git_commands :change default editor for git;
