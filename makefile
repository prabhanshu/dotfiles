Dir="~/dotfiles"

.PHONY: all

all: i3  emacs thunderbird bash slim config install

i3: ~/dotfiles/.i3
	ln --backup -s ~/dotfiles/.i3 ~/
	sudo ln -sf ~/dotfiles/.i3/i3status.conf /etc/i3status.conf

emacs:~/dotfiles/.emacs.d
	ln --backup -s ~/dotfiles/.emacs.d ~/
	sudo ln --backup -sf ~/dotfiles/.emacs.d /root/

thunderbird:~/dotfiles/.thunderbird
	ln --backup -sf ~/dotfiles/.thunderbird ~/

bash:~/dotfiles/.bashrc
	ln --backup -sf ~/dotfiles/.bashrc ~/
	ln --backup -sf ~/dotfiles/.inputrc ~/
	ln --backup -sf ~/dotfiles/.bash_profile ~/
	ln --backup -sf ~/dotfiles/.xinitrc ~/
	ln --backup -sf ~/dotfiles/.Xresources ~/
	ln --backup -sf ~/dotfiles/.vimrc ~/
	sudo ln --backup -sf ~/dotfiles/.vimrc /root/
	sudo ln --backup -sf ~/dotfiles/.bashrc /root/
	sudo ln --backup -sf ~/dotfiles/.inputrc /root/
	sudo ln --backup -sf ~/dotfiles/.bash_profile /root/
	sudo ln --backup -sf ~/dotfiles/.xinitrc /root/
	sudo ln --backup -sf ~/dotfiles/.Xresources /root/

slim:~/dotfiles/slim
	ln --backup -s ~/dotfiles/slim/slim.conf /etc/slim.conf
	sudo ln --backup -sf ~/dotfiles/slim/theme /usr/share/slim/theme

#config:~/dotfiles/.config
#	ln --backup -s ~/dotfiles/.config ~/.config

install:
	sudo pacman -S unzip openssh xclip git zathura zathura-pdf-poppler feh scrot wget 
