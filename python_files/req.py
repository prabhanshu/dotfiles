import requests
import json
from xmlutils.xml2json import xml2json
from xml.etree import ElementTree

header={"Host": "www.traffline.com",
        "X-Requested-With": "XMLHttpRequest",
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36",
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
}


#params="city=Delhi&source=Moti+Nagar+Metro+Station%2C+Najafgarh+Road%2C+DLE+Industrial+Area%2C+New+Delhi%2C+Delhi%2C+India&destination=West+Punjabi+Bagh%2C+New+Delhi%2C+Delhi%2C+India&sourceCords=(28.65787%2C+77.142674)&destinationCords=(28.6718527%2C77.1280004)&path="
params="city=Delhi&source=Moti+Nagar+Metro+Station+%2CNew+Delhi+%2CDelhi+%2CIndia&destination=West+Punjabi+Bagh+%2CNew+Delhi%2C+Delhi%2C+India&sourceCords=(28.65787%2C+77.142674)&destinationCords=(28.6718527%2C77.1280004)&path="

#params={"city":"Delhi","source":"Moti Nagar Metro Station,New Delhi,Delhi,India","destination":"West Punjabi Bagh,New Delhi,Delhi,India"}
url ="http://www.traffline.com/traffic_info"
r = requests.post(url, data=params, headers=header)
print r.text
print(r.url)
print r.encoding
print "\n \n \n"
#print r.content
#print "\n \n \n"
#print r.json()
#print "\n \n \n"
#print json.loads(r.text)
print json.dumps(r.text, sort_keys=True,
                  indent=4, separators=(',', ': '))


tree = ElementTree.fromstring(json.loads(r.text))
print type(tree)
print tree

# converter = xml2json(r.text, encoding="utf-8")
# print converter.get_json()
print r.status_code;
#xml.etree.ElementTree.Element
