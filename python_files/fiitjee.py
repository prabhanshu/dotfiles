import requests
import difflib
url="http://results.fiitjee-ftre.com/default.aspx"
header={
'Host': 'results.fiitjee-ftre.com',
'Connection': 'keep-alive',
'Content-Length': '401',
'Cache-Control': 'max-age=0',
'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
'Origin': 'http://results.fiitjee-ftre.com',
'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36',
'Content-Type': 'application/x-www-form-urlencoded',
'Referer': 'http://results.fiitjee-ftre.com/default.aspx',
'Accept-Encoding': 'gzip, deflate',
'Accept-Language': 'en-US,en;q=0.8',
'Cookie': '__atuvc=9%7C1; __atrfs=ab/|pos/|tot/|rsi/54b0717300000000|cfc/|hash/0|rsiq/|fuid/cdac5b4d|rxi/|rsc/|gen/3|csi/|dr/; sc_is_visitor_unique=rx10136880.1420902223.F56A96633E524F9AE4293E4B25A8245A.4.3.3.3.3.3.2.2.2; _gat=1; sc_is_visitor_unique=rx10136880.1420903342.F56A96633E524F9AE4293E4B25A8245A.4.3.3.3.3.3.2.2.2; _ga=GA1.2.287420963.1416760301'
}
data={
'__VIEWSTATE':'/wEPDwUJMTA2MDE5NDUyD2QWAgIDD2QWAgIJDxYCHglpbm5lcmh0bWxlZBgBBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WAgUMaW1nQnRuU3VibWl0BQtpbWdCdG5SZXNldKoeYFWjOHC9t+hAiQJQlkTkSHx7',
'__VIEWSTATEGENERATOR':'CA0B0334',
'__EVENTVALIDATION':'/wEWBQK7hqqjBALM2sHtDgKn+7qeAQLBwNgFAuyHtbUCzhG63YF+wBR/BODqFffMMdLtT7o=',
'txtRegistrationNo':'1001021252010130089',
'txtWAC':'23472',
'imgBtnSubmit.x':'67',
'imgBtnSubmit.y':'8'}

r=requests.post(url=url,data=data,headers=header)

with open("fiitjee.txt") as f, open(r.content) as g:
    flines = f.readlines()
    glines = g.readlines()

    d = difflib.Differ()
    diff = d.compare(flines, glines)
    print("\n".join(diff))
