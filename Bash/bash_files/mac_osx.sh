
#!/bin/bash
# installing mac os x theme

sudo add-apt-repository ppa:docky-core/ppa
sudo apt-get update
sudo apt-get install docky
sudo add-apt-repository ppa:noobslab/themes
sudo add-apt-repository ppa:noobslab/apps
sudo apt-get update
sudo apt-get install mac-ithemes-v3
sudo apt-get install mac-icons-v3
cd /usr/share/icons/mac-cursors && sudo ./uninstall-mac-cursors.sh
sudo apt-get remove mac-ithemes-v3 mac-icons-v3

sudo apt-get install mbuntu-bscreen-v3
# for removing mbuntu splash  sudo apt-get autoremove mbuntu-bscreen-v3
sudo apt-get install mbuntu-lightdm-v3
sudo apt-get install indicator-synapse
cd && wget -O Mac.po http://drive.noobslab.com/data/Mac-14.04/change-name-on-panel/mac.po
cd /usr/share/locale/en/LC_MESSAGES; sudo msgfmt -o unity.mo ~/Mac.po;rm ~/Mac.po;cd
wget -O launcher_bfb.png http://drive.noobslab.com/data/Mac-14.04/launcher-logo/apple/launcher_bfb.png
sudo mv launcher_bfb.png /usr/share/unity/icons/
sudo apt-get install unity-tweak-tool
wget -O mac-fonts.zip http://drive.noobslab.com/data/Mac-14.04/macfonts.zip
sudo unzip mac-fonts.zip -d /usr/share/fonts; rm mac-fonts.zip
sudo fc-cache -f -v
