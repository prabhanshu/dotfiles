#include <bits/stdc++.h> 
using namespace std;
#define ll long long
#define gc getchar_unlocked
#define pb push_back
#define MAX 10000
unsigned int arr[MAX];
int lastd(int n)
{
	return n%10;
}
int findd(int n)
{int count=0;
	while(n!=0)
		{n/=10;count++;}
return count;
}
int ipow(int base, int exp)
{
    int result = 1;
    while (exp)
    { if (exp & 1)
            result *= base;
        exp >>= 1;
        base *= base;
    }
return result;
}

void markMultiples(unsigned int arr[], int a, int n)
{
    int i = 2, num;
    while ( (num = i*a) <= n )
    {
        arr[ num-1 ] = 1; 
        ++i;
    }
}

void SieveOfEratosthenes(int n,std::vector<ll>  v)
{ int i;
    if (n >= 2)
    {
        
        memset(arr, 0, sizeof(arr));
        for (i=1; i<n; ++i)
        {
            if ( arr[i] == 0 )
            {
                v.pb(i+1);
                markMultiples(arr, i+1, n);
            }
        }
    }
}


int main()
{	
std::vector<ll> v;
SieveOfEratosthenes(MAX,v);
ll t;
int f,p,q,w,l;
char c;
cin>>t;
for (int i = 0; i < t; ++i)
{
	scanf("%d",&p);
	c=getchar();
	if(c=='.') {scanf("%d",&f);}
		else{printf("1\n");continue ;}
		q=findd(f);w=ipow(10,q);l=lastd(f);
		if(l==1 ||l==3 ||l==7 ||l==9) {printf("%d\n",w );continue;}
		
for (int j = 2; j <=MAX ; ++j)
{	
	if((j*f)%w ==0){printf("%d\n",j);break;}
}
       
}

return 0;
}