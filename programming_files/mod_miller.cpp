#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;
 
long long mulmod(long long a,long long b,long long c)
{   if(b==0) {return 1;}
    long long x = 0,y=a%c;
    while(b > 0)
    {
        if(b%2 == 1)
        {
            x = (x+y)%c;
        }
        y = (y*2)%c;
        b /= 2;
    }
    return x%c;
}

long long modulo(long long a,long long b,long long c){
    long long x=1,y=a; // long long is taken to avoid overflow of intermediate results
    while(b > 0){
        if(b%2 == 1){
            x=mulmod(x,y,c);
        }
        y = mulmod(y,y,c);// squaring the base
        b /= 2;
    }
    return x%c;
}
/* Miller-Rabin primality test, iteration signifies the accuracy of the test */
bool Miller(long long p)
{   int iter[9]={2,3,5,7,11,13,17,19,23};
    if(p<2){
        return false;
    }
    if(p!=2 && p%2==0){
        return false;
    }
    long long s=p-1;
    while(s%2==0){
        s/=2;
    }
    for(int i=0;i<9;i++)
    {
        long long a=iter[i],temp=s;
        long long mod=modulo(a,temp,p);
        while(temp!=p-1 && mod!=1 && mod!=p-1)
        {
            mod=mulmod(mod,mod,p);
            temp *= 2;
        }
        if(mod!=p-1 && temp%2==0){
            return false;
        }
    }
    return true;
}

int main(int argc, char const *argv[])
{   
  long long p;
  scanf("%lld",&p);
  if(Miller(p)){printf("P is a Prime\n");}
      else {printf("P is not Prime num\n");}

    return 0;
} 