#include <bits/stdc++.h> 
using namespace std;
#define pb push_back
#define ll long long

unsigned int arr[32000];
void markMultiples(unsigned int arr[], int a, int n)
{
    int i = 2, num;
    while ( (num = i*a) <= n )
    {
        arr[ num-1 ] = 1; 
        ++i;
    }
}

void SieveOfEratosthenes(int n,std::vector<ll> * v)
{ int i;
    if (n >= 2)
    {
        
        memset(arr, 0, sizeof(arr));
        for (i=1; i<n; ++i)
        {
            if ( arr[i] == 0 )
            {
                v->pb(i+1);
                markMultiples(arr, i+1, n);
            }
        }
    }
}
 

int main()
{
 clock_t tstart=clock();
 	int t,j;
 	ll n,m,count;
 	std::vector<ll> v;
 	SieveOfEratosthenes(32000,&v);

    double tp=(double)(clock()-tstart)/CLOCKS_PER_SEC;
    printf("\n%lf\n",tp);
 	scanf("%d",&t);
 	for (int i = 0; i < t; ++i)
 	{	count=0;
 		scanf("%lld",&m);
 		scanf("%lld",&n);
 		for (int i = m; i <=n ; ++i)
 		{
 			for (j = 0; j <v.size() ; ++j)
 			{
 				if(i%v[j]==0) {break;}
 			}
 			if(j==v.size()) {count++;}
 		}
 		printf("%lld\n",count );

 	}

    
    double temp=(double)(clock()-tstart)/CLOCKS_PER_SEC;
    printf("\n%lf\n",temp);
    return 0;
}