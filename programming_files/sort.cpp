#include <bits/stdc++.h>
#include <ctime> 
using namespace std;
#define ll long long
#define MAX 1000000
float s[MAX];
float m[MAX];
ll comq=0,comm=0;

void merge(float a[],float b[],int na,int nb)
{
	float c[na+nb];
	int pa=0,pb=0,pc=0;
	while(pa<na &&pb<nb)
		{	comm++;
			if(a[pa]<b[pb]) {c[pc]=a[pa];pa++;pc++;}
			else{c[pc]=b[pb];pb++;pc++;}
		}
		if(pa<na){while(pa<na) {c[pc]=a[pa];pa++;pc++;}}
		else if(pb<nb) {while(pb<nb) {c[pc]=b[pb];pb++;pc++;}}
		comm++;
		for (int i = 0; i < na+nb; ++i)
		{
			a[i]=c[i];
		}
		return;
	}

	void merge_sort(float a[],int i,int n)
	{	if(i>=n){return;}
	int j,mid;
	mid=(i+n)/2;
	merge_sort(a,i,mid);
	merge_sort(a,mid+1,n);
	merge(a+i,a+mid+1,mid-i+1,n-mid);
}


int part_tion(float A[],int l,int r){
	int i=l+1,j=r;comq+=(r-l);
	float x=A[l];
	while(j>=i){
		if(A[i]<=x)
			i++;
		else if(A[j]>x)
			j--;
		else{
			A[i]=A[i]+A[j];
			A[j]=A[i]-A[j];
			A[i]=A[i]-A[j];
			i++;
			j--;
		}
	}
	A[l]=A[j];
	A[j]=x;
	return j;
}
void quicksort(float A[],int l,int r)
{
	int p;
	if(r>l){
		p=part_tion(A,l,r);
		quicksort(A,l,p-1);
		quicksort(A,p+1,r);
		}
}


int main()
{   srand(time(0));
	srand(rand());
	int i;ll n;
	int t,q=0,me=0,beat=0;
	cin>>t;
	cin>>n;

	double countm=0;
	double countq=0;
	double tempq[100000];
	double tempm;
	int n0=0,n1=0,n2=0,n3=0,n5=0,n10=0;

	double a = 1.0;
	for (int j = 0; j < t; ++j)
	{	
		for (i=0;i<n;i++)
			{ m[i]=s[i]=((double)rand()/(double)(RAND_MAX)) * a; }

		clock_t tstart = clock();

		merge_sort(m,0,n-1);

		tempm= (double)(clock() - tstart)/CLOCKS_PER_SEC; 
		countm+=tempm;
		clock_t tstartq=clock();
		quicksort(s,0,n-1);
		tempq[j]= (double)(clock() - tstartq)/CLOCKS_PER_SEC;
		countq+=tempq[j];

		if(tempm <tempq[j]) {beat++;}

	}

	printf("AVERAGE QUICK SORT TIME: %lf\n",countq/t);
	printf("AVERAGE MERGE SORT TIME: %lf\n",countm/t );
	printf("AVERAGE QUICK comparisom: %lf\n",comq/(t*1.0) );
	printf("AVERAGE MERGE comparison: %lf\n",comm/(t*1.0) );
	printf("msort outperform qsort: %d\n",beat );
	printf("value of nlogn %lf  ,%lf\n",n*(log(n)*1.442695),n*(log(n)*1.442695)*1.39);

	for (int i = 0; i < t; ++i)
	{
	
		if(countq/t*1.05<=tempq[i]) {n0++;}
		if(countq/t*1.10<=tempq[i]) {n1++;}
		if(countq/t*1.20<=tempq[i]) {n2++;}
		if(countq/t*1.30<=tempq[i]) {n3++;}
		if(countq/t*1.5<=tempq[i]) {n5++;}
		if(countq/t*2.0<=tempq[i]) {n10++;}
	}
	
	cout<<">5% = "<<n0<<"   ";
	cout<<">10%= "<<n1<<"   ";
	cout<<">20%= "<<n2<<"   ";
	cout<<">30%= "<<n3<<"   ";
	cout<<">50%= "<<n5<<"   ";
	cout<<">100%= "<<n10<<"   ";

	return 0;

} 


