(require 'package) ;; You might already have this line
(package-initialize)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)

 ;;; Emacs Load Path
(load "~/.emacs.d/my-loadpackages.el")
(add-hook 'after-init-hook '(lambda ()
;;  (load "~/.emacs.d/my-noexternals.el")
))
     (setq load-path (cons "~/.emacs.d/elpa/" load-path))
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")

(set-frame-font "monospace 10")
(setq initial-scratch-message nil)

;;;; copy on select

(setq select-active-regions nil)
(setq mouse-drag-copy-region t)
(global-set-key [mouse-2] 'mouse-yank-at-click)

(global-unset-key "\C-z")
(flycheck-mode t)

;;(add-hook 'after-init-hook 'global-company-mode)

(electric-pair-mode 1)
(setq mouse-autoselect-window t)
;;https://github.com/magnars/multiple-cursors.el
(require 'multiple-cursors)
(global-set-key (kbd "C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C->") 'mc/mark-all-like-this)

(require 'smex)
(smex-initialize)
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
(global-set-key (kbd "<C-M-right>") 'forward-list)
(global-set-key (kbd "<C-M-left>") 'backward-list)

(setq inhibit-startup-message t
      inhibit-startup-echo-area-message t)

(setq next-line-add-newlines t)
(fset 'yes-or-no-p 'y-or-n-p)
;;(require 'recentf)
;;//(recentf-mode t)
; 50 files ought to be enough.
;;(setq recentf-max-saved-items 50)


(require 'ido)
(ido-mode t)
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)


(defun ido-recentf-open ()
  "Use `ido-completing-read' to \\[find-file] a recent file"
  (interactive)
  (if (find-file (ido-completing-read "Find recent file: " recentf-list))
      (message "Opening file...")
    (message "Aborting")))
;;(require 'yasnippet) ;; not yasnippet-bundle
;;(yas-global-mode 1)


(add-to-list 'load-path "~/.emacs.d/elpa/visual-regexp-20140926.408/") 
(require 'visual-regexp)
;;(global-unset-key "\C-s")
(define-key global-map (kbd "C-c r") 'vr/replace)
(define-key global-map (kbd "C-q") 'vr/query-replace)
;; if you use multiple-cursors, this is for you:
(define-key global-map (kbd "C-c m") 'vr/mc-mark)
(require 'dired )
(define-key dired-mode-map (kbd "RET") 'dired-find-alternate-file) ; was dired-advertised-find-file
(define-key dired-mode-map (kbd "^") (lambda () (interactive) (find-alternate-file "..")))  ; was dired-up-directory

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)

(add-hook 'LaTeX-mode-hook 'visual-line-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)

(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)

(setq TeX-PDF-mode t)

(require 'tramp)

(setq browse-url-browser-function 'w3m-browse-url)
(autoload 'w3m-browse-url "w3m" "Ask a WWW browser to show a URL." t)
;; optional keyboard short-cut
 (global-set-key "\C-xm" 'browse-url-at-point)
(setq w3m-use-cookies t)


(define-globalized-minor-mode global-highlight-parentheses-mode
  highlight-parentheses-mode
  (lambda ()
    (highlight-parentheses-mode t)))
(global-highlight-parentheses-mode t)

(global-set-key (kbd "C-x C-g") 'fasd-find-file)
(global-fasd-mode 1)
(setq fasd-enable-initial-prompt nil)

;; go lang
(add-hook 'before-save-hook 'gofmt-before-save)




(provide 'init)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (blackboard)))
 '(custom-safe-themes
   (quote
    ("458a5404ddea0362d3d2b565c1bdf524cf7198446a72cfa3ffb94a4601e4dfbd" "25523232df900eb1d155aaef33d7e80dfccfc6630f386e41dee968dd3a8caf90" "034bf00dfb00e47be7680024e38855eb33986c8146d493e9bf28de7855332049" "02955f318295c52b0d9ea82f8a1c0e786c7c392e0fc06509fc016797ba372a35" "7402b65eec929ee37e0bcee61256d9e9265246b92a9419e5d347ac762600a801" "4daae997e6f99bff8142e9af4df5a5af4d7a44236bd95a2a9a78e347894fbdda" "1284d7207fdc8d1170fe1b7aba80eb4d6a52b8f07ff923a334cb1db4f4e8d1bd" "30c1546ebbae463b183bbeaed22c81cebf65d11cfb23c31f52cbf59ea99544fe" "f641bdb1b534a06baa5e05ffdb5039fb265fde2764fbfd9a90b0d23b75f3936b" default)))
 '(fasd-completing-read-function (quote grizzl-completing-read))
 '(fasd-enable-initial-prompt nil)
 '(fasd-standard-search "-f"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'dired-find-alternate-file 'disabled nil)
