; loading package
(load "~/.emacs.d/my-packages.el")
(package-initialize)

(require 'visual-regexp)
;; ;(require 'magit)
;; ;(define-key global-map (kbd "C-c m") 'magit-status)

;; ;(require 'yasnippet)
;; ;(yas-global-mode 1)
;; ;(yas-load-directory "~/.emacs.d/snippets")
;; ;(add-hook 'term-mode-hook (lambda()
;;  ;   (setq yas-dont-activate t)));
